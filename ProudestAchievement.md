# Question 2
## Proudest Achievement

My proudest career achievement is replacing the central State Engine in the electronic FX trading system I worked on at BAML.
This was an enterprise scale project which I was the primary developer on from the design phase through to production go live and ongoing support.
As the project was replacing a legacy process which was part of an existing, intricate ecosystem, robustness of testing and deployment was paramount.

Issues with the old system had been a difficulty seeing the journey that customer requests followed through the system.
I discussed a new event driven approach utilising asynchronous queues with my manager.
Events on the queues could then be captured in new tables of our existing time series database for analysis of historical requests.

The more structured and accessible event logs made it easier for support to investigate specific requests, which in turn improved the client experience as any queries could be responded to quicker and more accurately.
Seeing this project go live without incident, and the increased ease of use of our system afterwards, was a very satisfying conclusion to several months of work.
