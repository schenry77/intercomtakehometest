# IntercomTakeHomeTest

This project is a code submission for the Take Home Test part of the Intercom job application process.

## Sample Output

The output for the sample `input/customers.txt` file can be found in `output/sorted_invitees.txt`

## How To Run

### Prerequisites

To run the unittests or code you will need Python installed.
The below instructions are based on the assumption that the `python` alias points to your Python installation.

This project was written with Python 3.8 core libraries.

### Running Code

From the directory: `IntercomTakeHomeTest/`

Run the command: `python inviteecalculator/main.py`

Or pass the path to InviteeCalculator.py to python if running from a different directory.

#### Specifying input and output files

By default, the program will read input from `input/customers.txt` and write output to `output/sorted_invitees.txt`

To specify different files you can set them using the `--input` and/or `--output` parameters in the command.

For example:

`python inviteecalculator/main.py --output output/new_output_file.txt`

### Running Tests

To run all tests in the project, from the directory: `IntercomTakeHomeTest`

Run the command: `python -m unittest`

## Code Structure

The `InviteeCalculator.py` script is the entry point for the program.

The `InviteeCalculator` class utilises dependency injection on creation to determine the functionality
for parsing input, filtering and sorting customers, and writing the output.

Dependency injection was used as the task specified to submit code as intended for production.

This way the functionality of `InviteeCalculator` could be extended or alternated easily by adding new classes to be
injected for each of these requirements.

Implemented in this project are the `JsonFileInputCustomerParser`, `CustomerFilter`, `InviteeSorter`
and `OutputFileWriter` classes.

Between them these classes provide the specific functionality required for the task in terms of the input and output 
being specified in txt files, the filtering done by location relative to the office, and the sorting done by `user_id`.

### Testing

The unit tests can be found in the `tests/` directory.

There are separate test files for each class meant to be injected into the main `InviteeCalculator` class,
as well as end-to-end tests using the injected classes for the `InviteeCalculator` class itself.

## Proudest Achievement paragraph

The paragraph on my proudest achievement for Part 2 of the test is in the `ProudestAchievement.md` file
in the root directory of this repository.