from unittest import TestCase

from unittest.mock import mock_open, patch

from inviteecalculator.OutputFileWriter import OutputFileWriter


class TestOutputFileWriter(TestCase):

    def test_write_output_uses_file(self):

        output_writer = OutputFileWriter('test_output_file.txt')
        invitees = [{'user_id': 123, 'name': "Joe Bloggs"}]

        with patch('builtins.open', mock_open()) as open_mock:
            output_writer.write_output(invitees)

            open_mock.assert_called_once_with('test_output_file.txt', 'w')

    def test_write_line_per_invitee(self):

        output_writer = OutputFileWriter('test_output_file.txt')
        invitees = [{'user_id': 123, 'name': "Joe Bloggs"},
                    {'user_id': 456, 'name': "Blog Joes"}]

        with patch('builtins.open', mock_open()):
            with patch('inviteecalculator.OutputFileWriter.OutputFileWriter._write_line') as write_line_mock:
                output_writer.write_output(invitees)

                self.assertEqual(2, write_line_mock.call_count)
