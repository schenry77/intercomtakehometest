import unittest
from inviteecalculator.CustomerFilter import CustomerFilter


class TestCustomerFilter(unittest.TestCase):

    def test_filter_by_distance_in_range(self):

        customer_filter = CustomerFilter()

        customers = [
            {
                'user_id': 4,
                'name': 'Ian Kehoe',
                'longitude': -6.238335,
                'latitude': 53.2451022
            }
        ]

        filtered_customers = customer_filter.filter_by_distance(customers, -6.257664, 53.339428, 100)

        self.assertEqual(len(filtered_customers), 1)

    def test_filter_by_distance_out_of_range(self):

        customer_filter = CustomerFilter()

        customers = [
            {
                'user_id': 21,
                'name': 'David Ahearn',
                'longitude': -9.442,
                'latitude': 51.802
            }
        ]

        filtered_customers = customer_filter.filter_by_distance(customers, -6.257664, 53.339428, 100)

        self.assertEqual(len(filtered_customers), 0)

