from unittest import TestCase
from inviteecalculator.InviteeSorter import InviteeSorter


class TestInviteeSorter(TestCase):

    def test_sort_by_user_id(self):

        unsorted_users = [
            {"latitude": "52.986375", "user_id": 12, "name": "Christina McArdle", "longitude": "-6.043701"},
            {"latitude": "51.92893", "user_id": 1, "name": "Alice Cahill", "longitude": "-10.27699"}
        ]

        invitee_sorter = InviteeSorter()

        sorted_users = invitee_sorter.sort_by_user_id(unsorted_users)

        self.assertEqual(sorted_users[0]["user_id"], 1)
        self.assertEqual(sorted_users[1]["user_id"], 12)
