import json
import os
import unittest

from inviteecalculator.JsonFileInputCustomerParser import JsonFileInputCustomerParser


class TestJsonInputCustomerParser(unittest.TestCase):

    def test_parse_input_file_not_found(self):

        test_input_file_path = self._test_input_file_path('not a real file name')

        parser = JsonFileInputCustomerParser(test_input_file_path)

        with self.assertRaises(FileNotFoundError):
            parser.parse_input()

    def test_parse_input_file_invalid_json(self):

        test_input_file_path = self._test_input_file_path('broken_json.json')

        parser = JsonFileInputCustomerParser(test_input_file_path)

        with self.assertRaises(json.JSONDecodeError):
            parser.parse_input()

    def test_parse_input_file_invalid_values(self):

        test_input_file_path = self._test_input_file_path('invalid_values.json')

        parser = JsonFileInputCustomerParser(test_input_file_path)

        with self.assertRaises(ValueError):
            parser.parse_input()

    def test_parse_input_file_missing_fields(self):

        test_input_file_path = self._test_input_file_path('missing_fields.json')

        parser = JsonFileInputCustomerParser(test_input_file_path)

        with self.assertRaises(KeyError):
            parser.parse_input()

    def test_parse_input_file_working_sample_to_list_of_dicts(self):

        test_input_file_path = self._test_input_file_path('working_sample.txt')

        parser = JsonFileInputCustomerParser(test_input_file_path)

        customers = parser.parse_input()

        self.assertTrue(isinstance(customers, list))
        self.assertTrue(isinstance(customers[0], dict))

    @staticmethod
    def _test_input_file_path(file_name: str) -> str:
        script_path = os.path.abspath(__file__)
        script_dir = os.path.split(script_path)[0]
        return os.path.join(script_dir, 'testinputfiles', file_name)
