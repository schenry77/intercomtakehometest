import os
from unittest import TestCase

from inviteecalculator.CustomerFilter import CustomerFilter
from inviteecalculator.InviteeCalculator import InviteeCalculator
from inviteecalculator.InviteeSorter import InviteeSorter
from inviteecalculator.JsonFileInputCustomerParser import JsonFileInputCustomerParser
from inviteecalculator.OutputFileWriter import OutputFileWriter


class TestInviteeCalculator(TestCase):
    """
    This test file is for end-to-end tests of the InviteeCalculator with dependencies injected
    For more specific edge case unit tests, see the test files for the corresponding injected classes
    """

    def test_calculate_invitees(self):

        input_file_path = self._test_file_path('working_sample.txt')
        test_output_file_path = self._test_file_path('test_output_file.txt')
        expected_output_file_path = self._test_file_path('expected_output.txt')

        invitee_calculator = InviteeCalculator(
            JsonFileInputCustomerParser(input_file_path),
            CustomerFilter(),
            InviteeSorter(),
            OutputFileWriter(test_output_file_path)
        )

        invitee_calculator.calculate_invitees(office_lon=-6.257664,
                                              office_lat=53.339428,
                                              distance_limit_km=100.0)

        # arguably, the file reading and writing should be mocked,
        # but this test case is aiming to be an end-to-end test
        with open(test_output_file_path, 'r') as output_file:
            output_file_lines = output_file.readlines()

        with open(expected_output_file_path, 'r') as expected_file:
            expected_file_lines = expected_file.readlines()

        for line_num, output_line in enumerate(output_file_lines):
            self.assertEqual(expected_file_lines[line_num], output_line)

    @staticmethod
    def _test_file_path(file_name: str) -> str:
        script_path = os.path.abspath(__file__)
        script_dir = os.path.split(script_path)[0]
        return os.path.join(script_dir, 'testinputfiles', file_name)
