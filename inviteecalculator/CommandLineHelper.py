import argparse
import os


class CommandLineHelper:

    @staticmethod
    def parse_args():
        arg_parser = argparse.ArgumentParser()
        arg_parser.add_argument("--input", help="Specify input file. Defaults to input/customers.txt")
        arg_parser.add_argument("--output", help="Specify output file. Defaults to output/sorted_invitees.txt")
        return arg_parser.parse_args()

    @staticmethod
    def input_file(args) -> str:
        file_path = CommandLineHelper._calculate_default_file_paths('../input/customers.txt')
        if args.input:
            print("Input file specified: " + args.input)
            file_path = args.input
        return file_path

    @staticmethod
    def output_file(args) -> str:
        file_path = CommandLineHelper._calculate_default_file_paths('../output/sorted_invitees.txt')
        if args.output:
            print("Output file specified: " + args.output)
            file_path = args.output
        return file_path

    @staticmethod
    def _calculate_default_file_paths(relative_file_path: str) -> str:
        script_path = os.path.abspath(__file__)
        script_dir = os.path.split(script_path)[0]
        return os.path.join(script_dir, relative_file_path)
