from typing import TextIO


class OutputFileWriter:

    def __init__(self, file_path):
        self.file_path = file_path

    def write_output(self, invitees):

        print('Writing output to file: ' + self.file_path)
        with open(self.file_path, 'w') as file:
            for invitee in invitees:
                self._write_line(invitee, file)

    @staticmethod
    def _write_line(invitee: dict, file: TextIO):
        line_to_output = str(invitee['user_id']) + ', ' + invitee['name'] + '\n'
        file.write(line_to_output)

