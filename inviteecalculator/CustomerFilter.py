from math import radians, sin, cos, acos


class CustomerFilter:

    EARTH_RADIUS_KM = 6371

    def filter_by_distance(self,
                           customers: [dict],
                           office_lon: float,
                           office_lat: float,
                           distance_limit_km: float):

        return list(filter(
            lambda customer: self._customer_within_range(customer, office_lon, office_lat, distance_limit_km),
            customers
        ))

    def _customer_within_range(self,
                               customer: dict,
                               office_lon: float,
                               office_lat: float,
                               distance_limit_km: float):

        distance_to_office_km = self._calculate_great_circle_distance(customer['longitude'],
                                                                      customer['latitude'],
                                                                      office_lon,
                                                                      office_lat)
        return distance_to_office_km < distance_limit_km

    def _calculate_great_circle_distance(self, a_lon_deg, a_lat_deg, b_lon_deg, b_lat_deg):

        # convert longitudes and latitudes from degrees to radians
        a_lon_rad, a_lat_rad, b_lon_rad, b_lat_rad = map(radians,
                                                         [a_lon_deg, a_lat_deg, b_lon_deg, b_lat_deg])

        # calculate central angle with great circle formula
        central_angle = acos(
            sin(a_lat_rad) * sin(b_lat_rad) + cos(a_lat_rad) * cos(b_lat_rad) * cos(a_lon_rad - b_lon_rad)
        )

        # calculate distance over sphere in km based on central angle and radius of earth
        return self.EARTH_RADIUS_KM * central_angle
