class InviteeCalculator(object):

    def __init__(self, input_parser, customer_filter, invitee_sorter, output_writer):
        """
        Initialise an InviteeCalculator with the dependencies passed as parameters here

        :param input_parser: Class for parsing an input of a list of customers
        :param customer_filter: Class with functionality to filter customers based on location criteria
        :param invitee_sorter: Class with functionality to sort customers
        :param output_writer: Class for outputting the resulting data
        """
        self.input_parser = input_parser
        self.customer_filter = customer_filter
        self.invitee_sorter = invitee_sorter
        self.output_writer = output_writer

    def calculate_invitees(self,
                           office_lon: float,
                           office_lat: float,
                           distance_limit_km: float):
        """
        Parses an input of customers and writes a sorted, filtered list of invitees to the output.
        The means of parsing the input, filtering and sorting the customers, and writing the output, are based on the
        injected dependencies

        :param office_lon: (float) The longitude of the office in degrees
        :param office_lat: (float) The latitude of the office in degrees
        :param distance_limit_km: (float) The maximum distance (in km) that a customer can be from the office to be invited
        """
        try:
            customers = self.input_parser.parse_input()

            invitees = self.customer_filter.filter_by_distance(customers, office_lon, office_lat, distance_limit_km)

            sorted_invitees = self.invitee_sorter.sort_by_user_id(invitees)

            self.output_writer.write_output(sorted_invitees)

        except Exception as ex:
            print('Error calculating invitees: ' + str(ex))

