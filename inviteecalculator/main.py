from CommandLineHelper import CommandLineHelper
from CustomerFilter import CustomerFilter
from InviteeCalculator import InviteeCalculator
from InviteeSorter import InviteeSorter
from JsonFileInputCustomerParser import JsonFileInputCustomerParser
from OutputFileWriter import OutputFileWriter


if __name__ == '__main__':
    """
    This main function serves as an entry point to the program for using the InviteeCalculator directly.
    Command line arguments are accepted for the input and output files for ease of testing with different files.
    The office and distance parameters are set as defined in the Intercom Take Home Test specification.
    """

    # use command line arguments for input and output files if specified
    command_line_args = CommandLineHelper.parse_args()
    input_file_path = CommandLineHelper.input_file(command_line_args)
    output_file_path = CommandLineHelper.output_file(command_line_args)

    # set office and distance parameters to be used
    office_longitude = -6.257664
    office_latitude = 53.339428
    distance_limit_km = 100

    # initialise InviteeCalculator with required dependencies
    invitee_calculator = InviteeCalculator(
        JsonFileInputCustomerParser(input_file_path),
        CustomerFilter(),
        InviteeSorter(),
        OutputFileWriter(output_file_path)
    )

    # calculate invitees with set parameters and dependencies
    invitee_calculator.calculate_invitees(office_lon=office_longitude,
                                          office_lat=office_latitude,
                                          distance_limit_km=distance_limit_km)