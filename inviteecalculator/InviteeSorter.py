class InviteeSorter:

    def sort_by_user_id(self, invitees: [dict]):
        return sorted(invitees, key=lambda user: user['user_id'])
