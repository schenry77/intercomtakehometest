import json


class JsonFileInputCustomerParser:

    def __init__(self, file_path: str):
        self.file_path = file_path

    def parse_input(self) -> [dict]:

        parsed_customers = []

        try:
            print('Reading input from: ' + self.file_path)
            with open(self.file_path, 'r') as file:
                for line in file:
                    raw_json = self._parse_json_object(line)
                    self._validate_json(raw_json)
                    parsed_customer = self._map_to_usable_types(raw_json)
                    parsed_customers.append(parsed_customer)

            return parsed_customers

        except FileNotFoundError as file_not_found_ex:
            print('Unable to find file: ' + self.file_path)
            raise file_not_found_ex

    def _parse_json_object(self, line: str) -> dict:
        try:
            return json.loads(line)

        except json.JSONDecodeError as json_decode_error:
            print('Error parsing JSON: ' + json_decode_error.msg)
            raise json_decode_error

    def _validate_json(self, raw_json: dict):

        for required_field in ['user_id', 'name', 'longitude', 'latitude']:
            if required_field not in raw_json:
                raise KeyError('Missing required field: ' + required_field)

    def _map_to_usable_types(self, raw_json: dict) -> dict:

        try:
            raw_json['longitude'] = float(raw_json['longitude'])
        except ValueError:
            print('Invalid value for longitude: ' + raw_json['longitude'])
            raise

        try:
            raw_json['latitude'] = float(raw_json['latitude'])
        except ValueError:
            print('Invalid value for longitude: ' + raw_json['latitude'])
            raise

        return raw_json
